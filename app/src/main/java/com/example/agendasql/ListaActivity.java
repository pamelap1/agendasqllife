package com.example.agendasql;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

import database.AgendaContacto;
import database.Contacto;


public class ListaActivity extends android.app.ListActivity {
    private MyArrayAdapter adapter;
    private ArrayList<Contacto> listaContacto;
    private Button btnnuevo;
    private AgendaContacto agendaContacto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);
        btnnuevo = (Button) findViewById(R.id.btnNuevo);
        agendaContacto = new AgendaContacto(this);

        llenarLista();
        adapter = new MyArrayAdapter(this,R.layout.activity_contacto,listaContacto);
        String str = adapter.contactos.get(1).getNombre();
        setListAdapter(adapter);

        btnnuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
    private void llenarLista() {
        agendaContacto.openDatabse();
        listaContacto = agendaContacto.allContactos();
        agendaContacto.cerrar();
    }

    class MyArrayAdapter extends ArrayAdapter<Contacto> {
        private Context context;
        private int texViewResourceId;
        private ArrayList<Contacto> contactos;

        public MyArrayAdapter(@NonNull Context context, int resource, ArrayList<Contacto> contactos) {
            super(context,resource,contactos);
            this.context = context;
            this.texViewResourceId = resource;
            this.contactos = contactos;
        }

        @NonNull
        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.texViewResourceId, null);
            TextView lblNombre = (TextView) view.findViewById(R.id.lblNombreContacto);
            TextView lblTelefono = (TextView) view.findViewById(R.id.lblTelefonoContacto);
            Button btnmodificar = (Button) view.findViewById(R.id.btnmodificar);
            Button btnBorrar = (Button) view.findViewById(R.id.btnborrar);

            if (contactos.get(position).getFavorite() > 0) {
                lblNombre.setTextColor(Color.BLUE);
                lblTelefono.setTextColor(Color.BLUE);
            }
            else {
                lblNombre.setTextColor(Color.BLACK);
                lblTelefono.setTextColor(Color.BLACK);
            }

            lblNombre.setText(contactos.get(position).getNombre());
            lblTelefono.setText(contactos.get(position).getTelefono1());

            btnmodificar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("contacto", contactos.get(position));
                    Intent i = new Intent();
                    i.putExtras(bundle);
                    setResult(Activity.RESULT_OK,i);
                    finish();
                }
            });

            btnBorrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    agendaContacto.openDatabse();
                    agendaContacto.eliminarContacto(contactos.get(position).get_ID());
                    agendaContacto.cerrar();
                    contactos.remove(position);
                    notifyDataSetChanged();
                }
            });

            return view;
        }
    }
}
